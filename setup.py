# coding=utf-8

import os

from setuptools import setup, find_packages

current_dir = os.path.dirname(__file__)
with open(os.path.join(current_dir, "requirements.txt")) as f:
    requirements = f.read().splitlines()

setup(
    name=u"ifbot",
    version='0.0.6',
    description="IFB gitlab bot",
    author="Julien Seiler",
    author_email="seilerj@igbmc.fr",
    license="GNU GENERAL PUBLIC LICENSE v2",
    url="",
    zip_safe=False,
    install_requires=requirements,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        'Natural Language :: English',
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],
    packages=find_packages(),
    package_dir={'ifbot': 'ifbot'},
    include_package_data=True
)
