import json

from ifbot.app import app, start_mq_connection
from ifbot.linter import lint_tools_changes

@app.cli.command(help="Listen to RabbitMQ message")
def consume():
    channel = start_mq_connection()

    channel.queue_declare(queue='merge_request_cluster_tools')

    def on_cluster_tools_merge_request(ch, method, properties, body):
        data = json.loads(body)
        lint_tools_changes(app, data)

    channel.basic_consume(queue='merge_request_cluster_tools',
                          auto_ack=True,
                          on_message_callback=on_cluster_tools_merge_request)

    channel.start_consuming()
