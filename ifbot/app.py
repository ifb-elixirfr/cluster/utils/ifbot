import json
import pika

from flask import abort, Flask, request
from ifbot import __version__
from ifbot.config import config

app = Flask(__name__)
app.config.from_mapping(config)


@app.route("/")
def home():
    return "I'm IFBot {version}".format(version=__version__)


@app.route("/gitlab", methods=['POST'])
def gitlab():

    if request.headers['X-Gitlab-Token'] != app.config['WEBHOOK_SECRET_TOKEN']:
        abort(403)

    channel = start_mq_connection()

    data = request.json
    if 'object_kind' not in data:
        abort(400)

    # new MR
    if data['object_kind'] == 'merge_request'\
       and data['project']['id'] == 15693267\
       and data['object_attributes']['action'] == 'open':
        print("Queuing MR !")
        queue = 'merge_request_cluster_tools'
        channel.queue_declare(queue=queue)
        channel.basic_publish(exchange='',
                              routing_key=queue,
                              body=json.dumps(data['object_attributes']))
    # new comment on MR
    if data['object_kind'] == 'note'\
       and data['project_id'] == 15693267\
       and data['object_attributes']['noteable_type'] == 'MergeRequest'\
       and data['user']['username'] != 'ifbot':
        print(data['user']['username'])
        print(data['object_attributes']['author_id'])
        if '@ifbot' in data['object_attributes']['note'] and 'check this' in data['object_attributes']['note']:
            print("Queuing MR !")
            queue = 'merge_request_cluster_tools'
            channel.queue_declare(queue=queue)
            channel.basic_publish(exchange='',
                                  routing_key=queue,
                                  body=json.dumps(data['merge_request']))

    return ""


def start_mq_connection():
    connection = pika.BlockingConnection(pika.ConnectionParameters(app.config['RABBITMQ_HOST']))
    return connection.channel()
