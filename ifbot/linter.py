import git
import gitlab
import os.path
import pkg_resources
import requests
import shutil
import tempfile
import yamale
import yaml

from jinja2 import Environment, PackageLoader


def lint_tools_changes(app, merge_request):

    target_project_id = merge_request['target_project_id']
    source_project_id = merge_request['source_project_id']
    merge_request_iid = merge_request['iid']
    source_branch = merge_request['source_branch']
    source_url = merge_request['source']['http_url']
    # target_branch = merge_request['target_branch']

    gl = gitlab.Gitlab('https://gitlab.com', private_token=app.config['GITLAB_PRIVATE_TOKEN'])
    target_project = gl.projects.get(target_project_id)
    source_project = gl.projects.get(source_project_id)
    mr = target_project.mergerequests.get(merge_request_iid)
    allow_push = target_project_id == source_project_id or mr.allow_maintainer_to_push
    changes = [change['new_path'] for change in mr.changes()['changes']]

    workflow_changes = []
    invalid_files = []
    changed_tools = {}

    for change in changes:
        # checking changes outside tools
        if not change.startswith('tools'):
            workflow_changes.append(change)
        else:
            if change.count('/') < 3:
                invalid_files.append(change)
            else:
                items = change.split('/')
                software = items[1]
                version = items[2]
                if software != 'sample':
                    if software not in changed_tools:
                        changed_tools[software] = set()
                    changed_tools[software].add(version)

    trigger_ci = True

    note = "Hi there!\n\n"
    note += "I just wanted to let you know that I linted all changes in your MR.\n\n"

    if not allow_push:
        note += ":warning: I'm not allowed to push on your branch, so I won't be able to auto-generate missing or incomplete files\n"
        note += "If you want me to update your branch, please tick the box \"Allow commits from members who can merge to the target branch.\" in your MR settings.\n\n"

    if len(workflow_changes) > 0:
        note += ":warning: Some files have been modified outside the tools directory:  \n"
        for change in workflow_changes:
            note += "  * {}\n".format(change)
        note += "\n"
    if len(invalid_files) > 0:
        note += ":bomb: Some files have been added outside the standard tools hierarchy:  \n"
        for change in invalid_files:
            note += "  * {}\n".format(change)
        note += "\n"
        trigger_ci = False

    if len(changed_tools) > 0:
        # clone repository
        repo_path = tempfile.mkdtemp()
        repo = git.Repo.clone_from(url=source_url, to_path=repo_path)
        repo.git.checkout("-b", "remote", "origin/{}".format(source_branch))

        for software in changed_tools.keys():
            for version in changed_tools[software]:
                note += "**{} v{}**  \n".format(software, version)

                meta_path = os.path.join(repo_path, 'tools', software, version, 'meta.yml')
                env_path = os.path.join(repo_path, 'tools', software, version, 'env.yml')

                if os.path.exists(meta_path) and has_basic_meta_content(meta_path):
                    # BASIC META DATA
                    with open(meta_path) as meta_stream:
                        meta = yaml.load(meta_stream)
                        deployment = meta['deployment']
                    if deployment == 'conda':
                        # CONDA
                        if os.path.exists(env_path):
                            if not is_valid_env(env_path, software, version):
                                # INVALID CONDA ENV DEFINITION
                                note += ":bomb: Invalid conda environment definition  \n"
                                trigger_ci = False
                                continue
                        else:
                            if not allow_push or not generate_conda_env(software, version, source_project, source_branch):
                                # MISSING CONDA ENV DEFINITION AND UNNABLE TO GENERATE IT
                                note += ":bomb: Missing conda environment definition and unable to generate it  \n"
                                trigger_ci = False
                                continue
                            else:
                                note += ":sparkles: I added a conda environment definition  \n"
                                repo.git.pull()

                        if not has_full_conda_meta_content(meta_path):
                            if not allow_push or not get_meta_data_from_anaconda(software, version, env_path, source_project, source_branch, create=False):
                                # INCOMPLETE META DATA AND UNABLE TO COMPLETE THEM AUTOMATICALLY
                                note += ":bomb: Incomplete meta data and unable to complete them automatically  \n"
                                trigger_ci = False
                                continue
                            else:
                                note += ":sparkles: I added meta data  \n"
                                repo.git.pull()
                    else:
                        # SINGULARITY
                        image_path = os.path.join(repo_path, 'tools', software, version, 'image.def')
                        if not has_full_singularity_meta_content(meta_path):
                            # INCOMPLETE META DATA
                            note += ":bomb: Incomplete meta data and unable to complete them automatically  \n"
                            trigger_ci = False
                            continue
                        if not os.path.exists(image_path):
                            # MISSING SINGULARITY IMAGE DEFINITION
                            note += ":bomb: Missing singularity image definition  \n"
                            trigger_ci = False
                            continue
                else:
                    # NO META DATA
                    if os.path.exists(env_path):
                        # LOOKS LIKE CONDA DEPLOYMENT
                        if not allow_push or not get_meta_data_from_anaconda(software, version, env_path, source_project, source_branch, create=True):
                            # UNABLE TO GENERATE METADATA FROM CONDA ENV
                            note += ":bomb: Unable to generate meta data from conda environment  \n"
                            trigger_ci = False
                            continue
                        else:
                            note += ":sparkles: I added meta data  \n"
                    else:
                        # INCOMPLETE DEPLOYMENT DEFINITION
                        note += ":bomb: Incomplete deployment definition  \n"
                        trigger_ci = False
                        continue

                note += ":white_check_mark: Everything looks ok  \n"

        shutil.rmtree(repo_path)

    if not trigger_ci:
        note += "\nPlease review your MR."
    # else:
    #    note += "\nTriggering CI tests :rocket:"
    #
    #    trigger = get_or_create_trigger(source_project)
    #    source_project.trigger_pipeline(source_branch, trigger.token, variables={"TARGET_BRANCH": target_branch})

    mr.notes.create({'body': note})


def has_basic_meta_content(meta_path):
    """ Check if the meta data file repesct basic meta data schema """
    meta_schema = yamale.make_schema(pkg_resources.resource_filename('ifbot', 'schemas/basic_meta.yml'))
    data = yamale.make_data(meta_path)

    try:
        yamale.validate(meta_schema, data)
    except ValueError:
        return False

    return True


def has_full_conda_meta_content(meta_path):
    """ Check if the meta data file repesct conda meta data schema """
    meta_schema = yamale.make_schema(pkg_resources.resource_filename('ifbot', 'schemas/conda_meta.yml'))
    data = yamale.make_data(meta_path)

    try:
        yamale.validate(meta_schema, data)
    except ValueError:
        return False

    return True


def has_full_singularity_meta_content(meta_path):
    """ Check if the meta data file repesct singularity meta data schema """
    meta_schema = yamale.make_schema(pkg_resources.resource_filename('ifbot', 'schemas/singularity_meta.yml'))
    data = yamale.make_data(meta_path)

    try:
        yamale.validate(meta_schema, data)
    except ValueError:
        return False

    return True


def is_valid_env(env_path, software, version):
    """ Check if the env file respect conda env schema and is properly named """
    conda_env_schema = yamale.make_schema(pkg_resources.resource_filename('ifbot', 'schemas/conda_env.yml'))
    data = yamale.make_data(env_path)
    try:
        yamale.validate(conda_env_schema, data)
    except ValueError as err:
        print(err)
        return False

    with open(env_path) as env_stream:
        env = yaml.load(env_stream, Loader=yaml.FullLoader)

    if env['name'] != '{}-{}'.format(software, version):
        return False

    return True


def generate_conda_env(software, version, project, branch):
    """ Search anaconda for a package to deploy the given software version and create corresponding env file """

    # search anaconda for software
    search_response = requests.get("https://api.anaconda.org/search?name={}".format(software))
    if search_response.status_code != requests.codes.ok:
        return False

    owners = set()

    for package in search_response.json():
        print(package["name"])
        print(package["versions"])
        if package['name'] == software and version in package['versions'] and 'linux-64' in package['conda_platforms']:
            owners.add(package['owner'])

    # choose channel for deployment
    if 'bioconda' in owners:
        channels = ['conda-forge', 'bioconda', 'nodefaults']
        package = 'bioconda::{}={}'.format(software, version)
    elif 'conda-forge' in owners:
        channels = ['conda-forge', 'nodefaults']
        package = 'conda-forge::{}={}'.format(software, version)
    else:
        return False

    # commit a new conda env file
    template_env = Environment(loader=PackageLoader('ifbot', 'templates'))
    env_template = template_env.get_template('env.j2')

    data = {
        'branch': branch,
        'commit_message': "add conda env for {} {}".format(software, version),
        'actions': [
            {
                'action': 'create',
                'file_path': 'tools/{}/{}/env.yml'.format(software, version),
                'content': env_template.render(software=software, version=version, channels=channels, package=package)
            }
        ]
    }
    project.commits.create(data)

    return True


def get_meta_data_from_anaconda(software, version, env_path, project, branch, create=True):

    with open(env_path) as env_stream:
        env = yaml.load(env_stream, Loader=yaml.FullLoader)

    conda_package = None
    for package in env['dependencies']:
        if software in package:
            conda_package = package
            break

    if not conda_package:
        return False

    channel = None
    if '::' in conda_package:
        (channel, package) = conda_package.split('::')
    else:
        package = conda_package

    if '=' in package:
        (package, package_version) = package.split('=')
    else:
        package_version = version

    if channel is None:
        channels = env['channels']
    else:
        channels = [channel]

    description = None
    url = None
    for channel in channels:
        package_response = requests.get("https://api.anaconda.org/package/{}/{}".format(channel, package))
        if package_response.status_code == requests.codes.ok:
            information = package_response.json()
            if package_version in information['versions']:
                description = information.get('summary', None)
                url = information.get('url', None)
                break

    if not description:
        return False

    template_env = Environment(loader=PackageLoader('ifbot', 'templates'))
    meta_template = template_env.get_template('meta.j2')

    data = {
        'branch': branch,
        'commit_message': "add meta data for {} {}".format(software, version),
        'actions': [
            {
                'action': 'create' if create else 'update',
                'file_path': 'tools/{}/{}/meta.yml'.format(software, version),
                'content': meta_template.render(deployment='conda', description=description, url=url)
            }
        ]
    }
    project.commits.create(data)

    return True


def get_or_create_trigger(project):
    trigger_decription = 'deployment_test'
    for t in project.triggers.list():
        if t.description == trigger_decription:
            return t
    return project.triggers.create({'description': trigger_decription})
